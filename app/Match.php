<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/*
* Class representation of math
*/
class Match extends Model{
    /*
    1=X
    2=O
    */
    /**
     * String representation for binary combinations on the board that wins the game
     */
    private $winningCombinations = [
        '111000000',
        '000111000',
        '000000111',
        '100100100',
        '010010010',
        '001001001',
        '100010001',
        '001010100',
    ];
    
    /**
     * Checks if the game is over by winning
     *
     * @return boolean
     */
    public function checkWinner(){
        $board=json_decode($this->board);
        $player=$this->next;
        $check = [];
        //convert the moves of player $player to a binary string representation
        foreach ($board as $position => $value){
            $check[$position]=0;
            if ($value == $player){
                $check[$position]=1;
            }
        }
        //converts the string representation to a decimal number
        $checkDec = bindec((string)implode('',$check));
        foreach ($this->winningCombinations as $combo){
            $comboDec = bindec((string)$combo);
            /*
            * if one of the binary representation (converted to decimal) AND the moves on the board of player $player
            * is equal to the compared value , the game is over
            */
            if ( ($comboDec & $checkDec) == $comboDec){
                //we hace a winner
                return 1;
            }
        }
        //no winner yet
        return 0;
    }

    /*
    * Dummy function to check conversions from the board to a binary string
    */
    private function checkMatch($board, $player){
        $check = [];
        foreach ($board as $position => $value){
            $check[$position]=0;
            if ($value == $player){
                $check[$position]=1;
            }
        }    
        return (string)(implode('',$check));
    }
}
