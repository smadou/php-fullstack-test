<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Match;

class MatchController extends Controller {
    
    public function index() {
        return view('index');
    }

    /**
     * Returns a list of matches
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches() {
        $matches = Match::all();
        return response()->json($matches);
    }

    /**
     * Returns the state of a single match
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id) {
        $match = Match::find($id);
        if (!$match){
            abort(404);
        }
        $match["board"]=json_decode($match["board"]);
        return response()->json($match);
    }

    /**
     * Makes a move in a match
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id) {
        $match = Match::find($id);
        if (!$match){
            abort(404);
        }
        $board=json_decode($match["board"]);
        $position = Input::get('position');
        //checks if the position is not taken (hack alert)
        if ($board[$position]!==0){
            abort(401);
        }
        if ($match["next"]==1){
            $next=2;
            $board[$position] = 1;
        }else{
            $next=1;
            $board[$position] = 2;
        }
        $match["board"]=json_encode($board);
        //check winner
        $winner = $match->checkWinner();
        if ($winner==1){
            $match["winner"]=$match["next"];
        }
        $match["next"]=$next;
        //save the move
        if ($match->save()){
            return response()->json(
                [
                    'id' => $match["id"],
                    'name' => $match["name"],
                    'next' => $match["next"],
                    'winner' => $match["winner"],
                    'board' => json_decode($match["board"])
                ]
            );
        }else{
            abort(500);
        }
    }
    
    /**
     * Creates a new match and returns the new list of matches
     * @return \Illuminate\Http\JsonResponse
     */
    public function create() {
        if ($this->createOneMatch()){
            $matches = Match::all();
            return response()->json($matches);
        }else{
            abort(500);
        }
    }

    /**
     * Creates a new match and returns the id
     * @return Int id of the created match
     */
    public function createOneMatch(){
        $max_id = match::max('id');
        if ($max_id){
            $max_id+=1;
        }else{
            $max_id=1;
        }
        $match = new Match();
        $match->id = $max_id;
        $match->name = "Match$max_id";
        $match->next = mt_rand(1,2);
        $match->winner = 0;
        $match->board = '[0,0,0,0,0,0,0,0,0]';
        $match->created_at = date("Y-m-d H:i:s"); 
        if ($match->save()){
            return $max_id;
        }else{
            abort(500);
        }
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        $match = Match::find($id);
        if (!$match){
            abort(404);
        }
        if ($match->delete()){
            $matches = Match::all();
            return response()->json($matches);
        }else{
            abort(500);
        }
    }
}