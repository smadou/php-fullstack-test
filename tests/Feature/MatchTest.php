<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MatchTest extends TestCase{
    private $lastMatch = [];
    private $testCases = [
        [
            1, 0, 2,
            0, 1, 2,
            0, 0, 0,
        ],
        [
            1, 0, 2,
            1, 1, 2,
            0, 2, 0,
        ],
        [
            1, 2, 2,
            1, 1, 2,
            0, 0, 0,
        ],
        [
            1, 1, 2,
            1, 1, 2,
            2, 2, 0,
        ],
        [
            1, 2, 1,
            1, 1, 2,
            0, 2, 2,
        ],
    ];
    private $winCases = [
        [
            1, 0, 2,
            0, 1, 2,
            0, 2, 1,
        ],
        [
            1, 1, 2,
            1, 1, 2,
            0, 2, 2,
        ],
        [
            1, 2, 2,
            1, 1, 2,
            1, 2, 0,
        ],
    ];

    /**
     * Creates a match
     *
     * @return void
     */
    public function testCreateMatch(){
        $response = $this->post('/api/match');
        $response->assertStatus(200);
    }
    /**
     * Get All matches
     *
     * @return void
     */
    public function testGetMatches(){
        $response = $this->get('/api/match');
        $response->assertStatus(200);
        $matchArray = json_decode($response->content(), true);
        $this->lastMatch = array_pop($matchArray);
    }

    /**
     * Get last created match
     *
     * @return void
     */
    public function testGetLastMatch(){
        $this->testGetMatches();
        $response = $this->get('/api/match/'.$this->lastMatch["id"]);
        $response->assertStatus(200);
    }
    
    /**
     * Make a move in last created match
     *
     * @return void
     */
    public function testMoveLastMatch(){
        $this->testGetMatches();
        $response = $this->get('/api/match/'.$this->lastMatch["id"]);
        $response->assertStatus(200);
        $response = $this->put('/api/match/'.$this->lastMatch["id"], [
            'position' => 1,
        ]);
        $response->assertStatus(200);
    }
    /**
     * Delete last created match
     *
     * @return void
     */
    public function testDeleteLastMatch(){
        $this->testGetMatches();
        $response = $this->delete('/api/match/'.$this->lastMatch["id"]);
        $response->assertStatus(200);
    }

    /**
     * Get player Moves board
     * @return array 
     */
    private function getPlayerMoves($board, $player){
        $moves = [];
        foreach ($board as $position => $playerMove){
            if ($player==$playerMove){
                $moves[]=$position;
            }
        }
        return $moves;
    }

    /**
     * test play matches in test array
     *
     * @return void
     */
    public function testPlayMatches(){
        $this->playMatches($this->testCases, 0);
    }

    /**
     * test play matches in test array
     *
     * @return void
     */
    public function testPlayWinMatches(){
        $this->playMatches($this->winCases, 1);
    }

    /**
     * play matches in test array
     *
     * @return void
     */
    public function playMatches($testCases, $win){
        foreach ($testCases as $testCase){
            $this->testCreateMatch();
            $this->testGetMatches();
            $response = $this->get('/api/match/'.$this->lastMatch["id"]);
            $response->assertStatus(200);
            $this->lastMatch = json_decode($response->content(), true);
            $playerXMoves=$this->getPlayerMoves($testCase,1);
            $playerOMoves=$this->getPlayerMoves($testCase,2);
            for ($i=0; $i<sizeof($testCase); $i++){
                if (empty($playerOMoves)&&empty($playerXMoves)){
                    break;
                }
                $position = false;
                if ($this->lastMatch["next"]==1){
                    $position = array_pop($playerXMoves);
                }elseif ($this->lastMatch["next"]==2){
                    $position = array_pop($playerOMoves);
                }
                if ($position!==false){
                    $response = $this->put('/api/match/'.$this->lastMatch["id"], [
                        'position' => $position,
                    ]);
                    $response->assertStatus(200);
                }
                $response = $this->get('/api/match/'.$this->lastMatch["id"]);
                $response->assertStatus(200);
                $this->lastMatch = json_decode($response->content(), true);
            }
            if ($this->lastMatch["winner"]){
                $winner=1;
            }else{
                $winner=0;
            }
            $this->assertTrue($winner == $win);
        }
    }   
}